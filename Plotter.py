import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib as mpl
from sklearn.metrics import roc_curve, auc
from itertools import cycle
from tensorflow.keras import backend as K

mpl.rcParams["axes.labelsize"] = 15


def HeatmapCFA(factors, df, param, save):
    """
    Used in Step2.
    Plots heatmap of the parameters before vs after CFA.
    """
    loadings = factors.loadings_

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(28,10))
    sns.heatmap(np.cov(df.T), vmin=-1, vmax=1, cmap='RdBu', square=True, xticklabels=param, yticklabels=param, ax=ax1, cbar_kws={'fraction':0.046})
    sns.heatmap(loadings, vmin=-1, vmax=1, cmap='RdBu', square=True, xticklabels=['F1', 'F2', 'F3', 'F4', 'F5'], yticklabels=param, ax=ax2, cbar_kws={'fraction':0.70})
    cbar = ax1.collections[0].colorbar
    cbar.set_label(label='Covariance', size=15)
    cbar = ax2.collections[0].colorbar
    cbar.set_label(label=r'Factor loading $\theta$' , size=15)
    plt.subplots_adjust(left=0.055, right=1, bottom=0.165, top=0.855, hspace=0.2, wspace=0.035)
    if save:
        plt.savefig('Data/heatmaps.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def Pairgrid(code, output_dir, latent_dim, regularizer, optiName, save):
    """
    Used in Step3.
    Plots pairgrid of the hidden layers of the autoencoder.
    """
    g = sns.PairGrid(code, height=1.1, aspect=1)
    g = g.map_diag(sns.histplot, bins=50)
    g =  g.map_offdiag(sns.histplot, cmap='gnuplot2', cbar=True)
    g.fig.align_ylabels(g.axes[:, 0])
    g.fig.align_xlabels(g.axes[0, :])
    g.fig.set_size_inches(16, 10)
    plt.subplots_adjust(hspace=0.16, wspace=0.22, top=0.98, bottom=0.05, left=0.05, right=0.98)
    plt.margins(0, 0)
    if save:
        plt.savefig(output_dir+'selu_output_hiddenlayer_' + str(latent_dim) + '_' + str(
        regularizer) + '_' + optiName + '.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def Loss(output_dir, latent_dim, regularizer, optiName, nb_epochs, history, save):
    """
    Used in Step3.
    Plots training loss versus validation loss.
    """
    if nb_epochs != len(history.history['loss']):
        nb_epochs = len(history.history['loss'])
    plt.plot(np.arange(1, nb_epochs + 1, 1), history.history['val_loss'], label='Validation loss')
    plt.plot(np.arange(0.5, nb_epochs + 0.5, 1), history.history['loss'], label='Train loss')
    plt.legend()
    plt.yscale('log')
    plt.ylabel('Loss')
    plt.xlabel('Number of epochs')
    plt.show()
    if save:
        plt.savefig(output_dir+'selu_lossfunction_' + str(latent_dim) + '_' + str(
        regularizer) +'_'+optiName+ '.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()
    #save also the validation loss data, to plot against other losses for different hyperparameters
    if save:
        pd.DataFrame(history.history['val_loss']).to_csv(
        output_dir+'selu_lossfunction_' + str(latent_dim) + '_' + str(regularizer) + '_' + optiName + '.csv')


def Loss_vs_lr(output_dir, nb_epochs, history, save):
    """
    Used in Step3 Helper, to determine hyperparameter: learning rate.
    Plots training loss learning rate.
    """
    epochs = np.arange(1, nb_epochs+1, 1)
    plt.plot(1e-8*10**(epochs/20), history.history['val_loss'])
    plt.xscale('log')
    plt.yscale('log')
    plt.ylabel('Loss')
    plt.xlabel('Learning rate')
    if save:
        plt.savefig(output_dir+'Determine_lr.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def Loss_diff_lr(output_dir, save):
    """
    Used in Step3 Helper, to determine hyperparameter: learning rate.
    Plots validation loss for different values of the learning rate.
    """
    for latent_dim, regularizer, lr, optiName in zip([7,7,7,7],
        [0.03, 0.03, 0.03, 0.03],
        [0.00005, 0.0001, 0.0005, 0.001],
        ['adam0.00005', 'adam0.0001', 'adam0.0005', 'adam0.001']):
        loss = pd.read_csv(output_dir+'selu_lossfunction_'+str(latent_dim)+'_'+str(regularizer)+'_'+optiName+'.csv')
        plt.plot(loss['Unnamed: 0'], loss['0'], label=r'Adam $\alpha$='+str(lr))
    plt.yscale('log')
    plt.xlabel('Number of epochs')
    plt.ylabel('Validation loss')
    plt.legend()
    if save:
        plt.savefig(output_dir+'Determine_lr_loss.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def Activity_notZero(autoencoder, x_val, output_dir, latent_dim, regularizer, optiName,  names_features, save):
    """
    Used in Step3 Helper, to determine hyperparameter: regularizer.
    - Plots histogram of amount of hidden nodes that were non-zero at the same time.
    - Plots percentage of activations that were non-zero for each hidden node in the autoencoder.
    """
    get_relu_output = K.function([autoencoder.layers[0].input], [autoencoder.layers[0].output])
    relu_output = get_relu_output([x_val])
    hist_activity_notzero = []
    for i in range(len(x_val)-1):
        count0 = 0
        count_not0 = 0
        for k in relu_output[0][i]:
            if k <= 0.01:
                count0 += 1
            else:
                count_not0 += 1
        hist_activity_notzero.append(count_not0)
    plt.hist(hist_activity_notzero, bins=np.arange(0, latent_dim+1, 1), align='right')
    plt.xlabel('Number of activated hidden nodes')
    plt.ylabel('Count')
    if save:
        plt.savefig(output_dir+'activity_hist_' + str(latent_dim) + '_' + str(
            regularizer) + '_' + optiName + '.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()

    #do reverse
    hist_activity_notzero_reverse = np.zeros(latent_dim)
    hist_activity_zero_reverse = np.zeros(latent_dim)
    for i in range(len(x_val)-1):
        for number, k in enumerate(relu_output[0][i]):
            if k <= 0.01:
                hist_activity_zero_reverse[number] += 1
            else:
                hist_activity_notzero_reverse[number] += 1
    plt.scatter(names_features[:latent_dim], hist_activity_notzero_reverse/len(x_val)*100, s=50, marker='X')
    plt.ylim(0, 100)
    plt.xlabel('Hidden node number')
    plt.ylabel(r'Percentage of non-zero activity [$\%$]')
    if save:
        plt.savefig(output_dir+'AE_activity_notZero_otherwayround_' + str(
        latent_dim) + '_' + str(regularizer) + '_' + optiName + '.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def ErrorKNN(nb, error, poly4, output_dir, save):
    """
    Used in Step4, classification Helper, to plot the error of different runs of KNN.
    Helps to determine the optimal number of neighbours.
    """
    xvalues = np.arange(1, nb-0.9, 0.1)
    plt.figure()
    plt.plot(range(1,nb), error, color='black', linestyle='dashed', marker='o', markerfacecolor='grey', markersize=10)
    plt.plot(xvalues, poly4, label='4th order')
    plt.xlabel('K value', fontsize=20)
    plt.ylabel('Mean error', fontsize=20)
    plt.xticks(range(1, nb))
    plt.legend()
    if save:
        plt.savefig(output_dir + 'KNN_optimalNeighbours.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def ROC_curve(pred_list, ytest, string, neighbours, output_dir, save):
    """
    Used in Step4, classification Helper, to plot the ROC curve of the KNN output.
    """
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(12,12))

    for ax_i, pred, title in zip([ax1, ax2, ax3, ax4], pred_list, ['1 neighbour', '3 neighbours', '6 neighbours', '10 neighbours']):
        pred_bin = []
        for i in range(len(pred)):
            for index, flare in enumerate(['C', 'M', 'No', 'X']):
                if ytest[i] == flare:
                    selected = pred[i, index]
                    rest = np.sum(np.delete(pred[i], index))
                    pred_bin.append([selected, rest])
        pred_x, pred_m, pred_c, pred_no = [], [], [], []
        ytest_x, ytest_m, ytest_c, ytest_no = [], [], [], []
        for str, list_pred, list_test in zip(['X', 'M', 'C', 'No'], [pred_x, pred_m, pred_c, pred_no], [ytest_x, ytest_m, ytest_c, ytest_no]):
            for i in range(len(ytest)):
                if ytest[i] == str:
                    list_pred.append(pred_bin[i][0])
                    list_test.append(1)
                else:
                    list_pred.append(pred_bin[i][1])
                    list_test.append(0)
        pred_x = np.array(pred_x)
        pred_m = np.array(pred_m)
        pred_c = np.array(pred_c)
        pred_no = np.array(pred_no)
        ytest_x = np.array(ytest_x)
        ytest_m = np.array(ytest_m)
        ytest_c = np.array(ytest_c)
        ytest_no = np.array(ytest_no)

        fpr = dict()
        tpr = dict()
        roc_auc = dict()

        flare_name = ['X', 'M', 'C', 'No']
        for i, list_pred, list_y in zip([0,1,2,3], [pred_x, pred_m, pred_c, pred_no], [ytest_x, ytest_m, ytest_c, ytest_no]):
            fpr[i], tpr[i], thresholds = roc_curve(list_y, list_pred)
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        all_fpr = np.unique(np.concatenate([fpr[i] for i in range(4)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(4):
            mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])
        mean_tpr /= 4

        fpr["macro"] = all_fpr
        tpr["macro"] = mean_tpr
        roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

        #plt.figure()
        ax_i.plot(fpr["macro"], tpr["macro"], label='Macro-average ROC curve (area = {0:0.2f})'.format(roc_auc["macro"]), 
                color='navy', linestyle=':', linewidth=4)
        colors = cycle(['aqua', 'darkorange', 'cornflowerblue', 'maroon'])
        linestyles = cycle(['solid', 'dotted', 'dashed', 'dashdot'])
        for i, color, linestyle in zip(range(4), colors, linestyles):
            ax_i.plot(fpr[i], tpr[i], color=color, lw=2, linestyle=linestyle, label='ROC curve of class {0} (area = {1:0.2f})'.format(flare_name[i], roc_auc[i]))
        ax_i.plot([0,1],[0,1], 'k--', lw=2)
        ax_i.set_xlim([0,1])
        ax_i.set_ylim([0, 1.05])
        ax_i.set_xlabel('False Positive Rate')
        ax_i.set_ylabel('True Positive Rate')
        ax_i.set_title(title, fontsize=18)
        ax_i.legend(loc='lower right')
    plt.subplots_adjust(left=0.057, right=0.979, bottom=0.057, top=0.952, hspace=0.29, wspace=0.23)
    if save:
        plt.savefig(output_dir+'kNN_ROC_curve_'+string+'_ALLneighbours.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def BIC_GMM(n_clusters, bics, bics_error, output_dir, save):
    """
    Used in Step 4, classification Helper, to determine the optimal number of clusters for GMM.
    """
    plt.errorbar(n_clusters, bics, yerr=bics_error, label='BIC')
    plt.title("BIC scores", fontsize=20)
    plt.xticks(n_clusters)
    plt.xlabel('Nb clusters')
    plt.ylabel("Score")
    plt.legend()
    if save:
        plt.savefig(output_dir+'NumberClustersGMM.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def Heatmap_nbFlareClusters(matrix, string, method, which, output_dir, save):
    """
    Used in Step4, classification Helper, to plot the output of K-means and GMM.
    The percentage of each flare type in each cluster is plotted here with a heatmap. 
    """
    ylabels = []
    for i in range(len(matrix)):
        ylabels.append('Cluster'+str(i+1))
    plt.figure(figsize=(8,1.5*len(matrix)))
    im = sns.heatmap(matrix, cmap='Oranges',
                     xticklabels=['No', 'C', 'M', 'X'], 
                     vmin=0, vmax=1, annot=True)
    plt.yticks(np.arange(len(matrix)) + 0.5, ylabels, fontsize="12",
               va="center")
    if save:
        plt.savefig(output_dir+'heatmap_'+method+'_'+string+'_'+str(which)+'.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()



def Heatmap_nbFlareClusters_KmeansGMM(matrix1, matrix2, string, which, output_dir, save):
    """
    Used in Step4, classification Helper, to plot the output of K-means and GMM together.
    """
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12,5))

    ylabels = []
    for i in range(len(matrix1)):
        ylabels.append('Cluster'+str(i+1))
    im = sns.heatmap(matrix1, cmap='Oranges',
                     xticklabels=['No', 'C', 'M', 'X'], 
                     vmin=0, vmax=1, annot=True, ax=ax1, square=True)
    ax1.set_yticks(np.arange(len(matrix1)) + 0.5, ylabels, fontsize="12",
               va="center")
    ax1.set_title('K-means', fontsize=18)

    ylabels = []
    for i in range(len(matrix2)):
        ylabels.append('Cluster'+str(i+1))
    im = sns.heatmap(matrix2, cmap='Oranges',
                     xticklabels=['No', 'C', 'M', 'X'], 
                     vmin=0, vmax=1, annot=True, ax=ax2, square=True)
    ax2.set_yticks(np.arange(len(matrix2)) + 0.5, ylabels, fontsize="12",
               va="center")
    ax2.set_title('GMM', fontsize=18)
    plt.subplots_adjust(left=0.024, right=0.988, bottom=0.071, top=0.9, hspace=0.2, wspace=0.105)
    if save:
        plt.savefig(output_dir+'heatmap_'+string+'_'+str(which)+'.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()

"""
Used in Step4.
Plots the mean and standard deviation of the 7 parameters, per flare type.
"""
def Mean_sigma(df, df_flares, save):
    # Plotting
    param = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7']
    Flares = ['No', 'C', 'M', 'X']
    df_with_flares = pd.concat([df, df_flares], axis=1)
    plt.figure(figsize=(10, 6))
    for par, shift in zip(param, np.arange(-0.15, +0.2, 0.05)):
        mean = []
        sigma = []
        for flare in Flares:
            temp = []
            for i, x in enumerate(df_with_flares[par]):
                if df_with_flares.iloc[i]['FlareClass'] == flare:
                    temp.append(x)
            mean.append(np.mean(np.array(temp)))
            sigma.append(np.array(temp).std())
        x = np.arange(1, 5, 1)
        plt.errorbar(x + shift, mean, yerr=sigma, marker='X', markersize=10, label=par)  # data.c #linestyle='None'
    plt.xticks(x, Flares)
    plt.xlabel('Flare', fontsize=20)
    plt.ylabel(r'$\mu\pm\sigma$ of parameters', fontsize=20)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(fontsize=10, loc='upper left')
    if save:
        plt.savefig('Data/mean_sigma.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()


def Density(df, df_flares, sampled_adasyn, sampled_smote, sampled_svmsmote, save):
    # Plotting
    param = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7']
    Flares = ['No', 'C', 'M', 'X']
    df_with_flares = pd.concat([df, df_flares], axis=1)

    """      
    for par in param:
        sns.distplot(df[par], hist=True, kde=True, 
                    bins=int(180/5), color = 'darkblue', 
                    hist_kws={'edgecolor':'black'},
                    kde_kws={'linewidth': 4})
        sns.distplot(underS_overS_SMOTE6000[par], hist=True, kde=True, 
                    bins=int(180/5), color = 'red', 
                    hist_kws={'edgecolor':'black'},
                    kde_kws={'linewidth': 4})
        plt.title(par)
        plt.show()
    """
    for flare in Flares:
        f, ((ax1, ax2, ax3, ax4), (ax5, ax6, ax7, ax8)) = plt.subplots(2, 4, figsize=(16, 8))
    
        for par, axis in zip(param, [ax1, ax2, ax3, ax4, ax5, ax6, ax7]):
            before = df_with_flares[df_with_flares['FlareClass'] == flare]
            after_adasyn = sampled_adasyn[sampled_adasyn['FlareClass'] == flare]
            after_smote = sampled_smote[sampled_smote['FlareClass'] == flare]
           # after_kmeanssmote = sampled_kmeanssmote[sampled_kmeanssmote['FlareClass'] == flare]
            after_svmsmote = sampled_svmsmote[sampled_svmsmote['FlareClass'] == flare]

            sns.distplot(before[par], hist=False, kde=True, 
                    #bins=int(180/5), 
                    color = 'darkblue',
                   # hist_kws={'edgecolor':'black'},
                    kde_kws={'linewidth': 4}, label='Real data', ax=axis)
            #sns.distplot(after_adasyn[par], hist=False, kde=True, 
            #        #bins=int(180/5), 
            #        color = 'magenta',
            #        #hist_kws={'edgecolor':'black'},
            #        label='Sampled data Adasyn', ax=axis, kde_kws={'linewidth':3, 'linestyle':'-'})
            sns.distplot(after_smote[par], hist=False, kde=True, 
                    #bins=int(180/5), 
                    color = 'orange',
                    #hist_kws={'edgecolor':'black'},
                    label='Sampled data Smote', ax=axis, kde_kws={'linewidth':3, 'linestyle':'-'})
            #sns.distplot(after_kmeanssmote[par], hist=False, kde=True, 
            #        #bins=int(180/5), 
            #        color = 'g',
            #        #hist_kws={'edgecolor':'black'},
            #        kde_kws={'linewidth': 4}, label='Sampled data kmeans Smote', ax=axis)
            #sns.distplot(after_svmsmote[par], hist=False, kde=True, 
            #        #bins=int(180/5), 
            #        color = 'magenta',
            #        #hist_kws={'edgecolor':'black'},
            #        kde_kws={'linewidth': 4}, label='Sampled data borderline Smote', ax=axis)
        handles, labels = ax1.get_legend_handles_labels()
        f.legend(handles, labels, loc=(0.8, 0.35), fontsize=15)
        f.delaxes(ax8)
        plt.suptitle(flare+' Flares', fontsize=25)
        plt.show()
        #if save:
        #    plt.savefig('Data/Density_plots'+flare+'.jpg', format='jpg', dpi=300)
        plt.clf()
        plt.close()