# Identifying Solar Flares by Clustering Active Regions

## Description
This code is related to the following paper: (ADD)

Clustering active regions based on magnetic field data (from Angryk et al. 2020, based on SHARPs) + analyzing the relation between the clustering and the co-occurence of solar flares and their strength.

## Download Data
The data used in this project is an open source data set of Angryk et al. (2020). This data set consists of SHARPS, and some additional magnetic field parameters, integrated with information from solar flare catalogues.

The data can be downloaded through this link: https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/EBCFKM
Download the SWAN.tar.gz archive, which contains all the data, subdivided in five partitions.

This data is used in Step1 to configure one data frame containing all the data, AllData_sharp.csv. By default, this file is already included in the Data folder. If you want to generate it yourself, adapt the code or delete teh AllData_sharp.csv file and download the above SWAN data set.


## Necessary Packages
Python 3.10.8
- pandas (1.5.1)
- numpy (1.23.4)
- matplotlib (3.5.3)
- seaborn (0.12.0)
- scipy (1.9.3)
- hdbscan (0.8.29)
- factor_analyzer (0.3.2)
- imbalanced-learn (0.7.0)
- scikit-learn (1.0.2)
- tensorflow (2.10.0)

## Run code
The code is split into four main steps:
- Step 1 : Configuration of data set (removing NaNs, standardization, merging parameters)
- Step 2 : Transformation of data set (outlier detection, widening distributions, redundancy removal)
- Step 3 : Second transformation of data set (sparsification of features using autoencoders)
- Step 4 : Classification of active regions (sampling + clustering)

There are three helper files containing additional methods used in Step 3 and Step 4:
- Helper hyperparameters AE : contains 3 steps to determine the optimal hyperparameters, learning rate and regularization.
- Helper sampling methods : contains all sampling methods.
- Helper classification methods : contains clustering methods and the methods to determine the hyperparameters.

There is also one file that renders all the plots:
- Plotter.py

Finally, if you want to apply the learned algorithms on new data you have to use the pipeline.
At this point the data needs to be selected individually by the user and entered in the right format.
- pipeline_newdata.py


The different steps need to be run separately. The helper and plotter files are integrated into the code and do not need to be run explicitely.
However, the Helper_hyperparametersAE.py file does need to be run in case you want to apply the code on a different data set or make some alterations to the method. Run this file, look at the results to determine the optimal hyperparameters and adapt these in Step3.
```
#adapt line 17 in Step1 to the directory where you downloaded the data set from Angryk et al.
python Step1_configuringDataSet.py
python Step2_transformingDataSet.py
python Helper_hyperparametersAE.py
#analyse results from Helper file, adapt Step3 with optimal parameters
python Step3_transformingDataSet_AE.py
python Step4_clustering.py
```

## Authors and acknowledgment
(ADD)

## How to cite
Citation code: Baeke, H. (2022). Identifying Solar Flares by Clustering Active Regions [Code]. https://gitlab.com/hanneb/clustering_ar_sf_hbaeke.git.

Citation paper: (ADD)
