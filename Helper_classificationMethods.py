import pandas as pd
import matplotlib.pyplot as plt
import sklearn.metrics as sm
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib as mpl
from sklearn.mixture import GaussianMixture
import seaborn as sns
from sklearn.cluster import KMeans
import Plotter
import scipy
import os
import random
import pickle

mpl.rcParams["axes.labelsize"] = 15

#set seed to allow reproducible results
SEED = 1
np.random.seed(SEED)
os.environ['PYTHONHASSEED'] = str(SEED)
random.seed(SEED)


def KNN_OptimalNbNeighbours(x_train, flare_train, x_val, flare_val, output_dir, save):
    """
    Determine optimal number of neighbours for KNN
    """
    error = []
    nb = 16
    for i in range(1, nb):
        knn = KNeighborsClassifier(n_neighbors=i)
        knnfit = knn.fit(x_train, flare_train)
        pred_i2 = knnfit.predict(x_val)
        error.append(np.mean(pred_i2 != flare_val))

    p4 = np.polyfit(range(1, nb), error, 4)
    xvalues = np.arange(1, nb-0.9, 0.1)
    poly4 = p4[4]+xvalues*p4[3]+np.power(xvalues, 2)*p4[2]+np.power(xvalues,3)*p4[1]+np.power(xvalues,4)*p4[0]
    number = np.round(xvalues[scipy.signal.argrelextrema(poly4, np.less)[0][0]])
    print('nb optimal neighbours', np.round(xvalues[scipy.signal.argrelextrema(poly4, np.less)[0][0]])) #gives 9 as optimal number of clusters
    Plotter.ErrorKNN(nb, error, poly4, output_dir, save)

    return number


def kNN(df, df_flare, data_sampled, string, output_dir, save=True):
    """
    KNN algorithm.
    Number of neighbours determined based on non-sampled data. Actual KNN algorithm applied on sampled data.
    """
    ### SPLIT DATA IN TRAIN VALIDATION TEST SETS ###
    train_ratio = 0.6
    validation_ratio = 0.2
    test_ratio = 0.2
    # train is now 60% of the entire data set
    x_train, x_test, flare_train, flare_test = train_test_split(df, df_flare, test_size=1 - train_ratio, random_state=SEED, stratify=df_flare)
    # test is now 20% of the initial data set
    # validation is now 20% of the initial data set
    x_val, x_test, flare_val, flare_test = train_test_split(x_test, flare_test,
                                                            test_size=test_ratio / (test_ratio + validation_ratio),
                                                            random_state=SEED, stratify=flare_test)
    x_train = pd.DataFrame(x_train).reset_index().drop('index', axis=1)
    x_val = pd.DataFrame(x_val).reset_index().drop('index', axis=1)
    x_test = pd.DataFrame(x_test).reset_index().drop('index', axis=1)
    flare_train = pd.DataFrame(flare_train).reset_index()['FlareClass']
    flare_val = pd.DataFrame(flare_val).reset_index()['FlareClass']
    flare_test = pd.DataFrame(flare_test).reset_index()['FlareClass']

    nbNeighbors = KNN_OptimalNbNeighbours(x_train, flare_train, x_val, flare_val, output_dir, save)
    nbNeighbors = int(nbNeighbors)

    ### PERFORM KNN FOR FOUND NUMBER OF NEIGHBOURS ###  
    data_sampled0 = data_sampled.iloc[:,:-1]
    data_sampled_flares = data_sampled.iloc[:,-1]
    x_train, x_test, flare_train, flare_test = train_test_split(data_sampled0, data_sampled_flares, test_size=1 - train_ratio, random_state=SEED)
    x_val, x_test, flare_val, flare_test = train_test_split(x_test, flare_test,
                                                            test_size=test_ratio / (test_ratio + validation_ratio),
                                                            random_state=SEED)
    x_train = pd.DataFrame(x_train).reset_index().drop('index', axis=1)
    x_val = pd.DataFrame(x_val).reset_index().drop('index', axis=1)
    x_test = pd.DataFrame(x_test).reset_index().drop('index', axis=1)
    flare_train = pd.DataFrame(flare_train).reset_index()['FlareClass']
    flare_val = pd.DataFrame(flare_val).reset_index()['FlareClass']
    flare_test = pd.DataFrame(flare_test).reset_index()['FlareClass']

    print("========d_flare===========")
    print(f"{type(df_flare)}")
    print(f"{df_flare}")
    print(f"{df_flare.unique() = }")
    print(f"{df_flare.value_counts()['C'] = }")
    print(f"{df_flare.value_counts()['M'] = }")
    print(f"{df_flare.value_counts()['X'] = }")
    print(f"{df_flare.value_counts()['No'] = }")

    print("===========================")

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12,8))
    for neighbours, string, axes in zip([1,nbNeighbors], [' neighbour', ' neighbours'], [ax1, ax2]):
        classifier = KNeighborsClassifier(n_neighbors=neighbours)
        classifier.fit(x_train, flare_train)
        sm.plot_confusion_matrix(classifier, df, df_flare, labels=['X', 'M', 'C', 'No'], #was previously on the sampled data, now on all data
                                                    display_labels=['X', 'M', 'C', 'No'],
                                                    cmap='Oranges', normalize='true', ax=axes)
        axes.set_title('KNN with '+str(neighbours)+string, fontsize=15)
    plt.subplots_adjust(left=0.052, right=1, bottom=0.229, top=0.77, hspace=0.2, wspace=0.126)
    plt.show()
    if save:
        plt.savefig(output_dir+'kNN_confusion matrix_normalized_'+string+'_allneighbours.jpg', format='jpg', dpi=300)
    plt.clf()
    plt.close()

    #this is only for the 9 neighbours case
    flare_pred = classifier.predict_proba(x_test)
    #Plotter.ROC_curve(flare_pred, flare_test, string, str(neighbours), output_dir, save)


    #to plot the ROC curve for 1, 3, 6 and 10 nearest neighbours
    all_flare_pred = []
    for k in [1,3,6,10]:
        classifier = KNeighborsClassifier(n_neighbors=k)
        classifier.fit(x_train, flare_train)
        flare_pred = classifier.predict_proba(x_test)
        all_flare_pred.append(flare_pred)

    filename = 'Data/Model_KNN.sav'
    pickle.dump(classifier, open(filename, 'wb'))
    Plotter.ROC_curve(all_flare_pred, flare_test, string, str(k), output_dir, save)

    """
    #new, check results for one active region over time
    data_2014may = pd.read_csv('Data/data_2014may.csv')
    data_numFlareStamp_2014may = pd.read_csv('Data/data_numFlareStamp_2014may.csv')
    #data must first undergo all the processing steps, so I need the output from selu_output_hiddenLayer...
    out = classifier.predict(data_2014may.drop('Unnamed: 0', axis=1)) #, data_numFlareStamp_2014may['FlareClass'])
    print('output of knn on data of may 2014:', out)
    print('times', data_numFlareStamp_2014may['Timestamp'])
    from sklearn import preprocessing
    le = preprocessing.LabelEncoder()
    le.fit(['No', 'C', 'M', 'X'])
    print(le.classes_)
    print(le.transform(['X', 'C', 'M', 'No']))
    flares_numbers = le.transform(data_numFlareStamp_2014may['FlareClass'])
    flares_numbers = pd.DataFrame(flares_numbers)
    flares_numbers = flares_numbers.replace(0, 5)
    flares_numbers = flares_numbers.replace(2, 0)
    flares_numbers = flares_numbers.replace(1, 2)
    flares_numbers = flares_numbers.replace(5, 1)
    print(list(flares_numbers).count(5))
    flares_numbers_out = le.transform(out)
    flares_numbers_out = pd.DataFrame(flares_numbers_out)
    flares_numbers_out = flares_numbers_out.replace(0, 5)
    flares_numbers_out = flares_numbers_out.replace(2, 0)
    flares_numbers_out = flares_numbers_out.replace(1, 2)
    flares_numbers_out = flares_numbers_out.replace(5, 1)

    fig, ax = plt.subplots(1,1)
    plt.plot(data_numFlareStamp_2014may['Timestamp'], flares_numbers, label='True values')
    plt.plot(data_numFlareStamp_2014may['Timestamp'], flares_numbers_out, label='Prediction')
    plt.setp(ax.xaxis.get_majorticklabels(), rotation=-45, ha="left", rotation_mode="anchor") 
    x = np.arange(0, 4, 1)
    print(x)
    Flares = ['No', 'C', 'M', 'X']
    plt.yticks(x, Flares)

    plt.legend()
    plt.show()

    #new, check results for one active region over time
    data_2016may = pd.read_csv('Data/data_2016may.csv')
    data_numFlareStamp_2016may = pd.read_csv('Data/data_numFlareStamp_2016may.csv')
    #data must first undergo all the processing steps, so I need the output from selu_output_hiddenLayer...
    out = classifier.predict(data_2016may.drop('Unnamed: 0', axis=1)) #, data_numFlareStamp_2014may['FlareClass'])
    print('output of knn on data of may 2014:', out)

    le = preprocessing.LabelEncoder()
    le.fit(['No', 'C', 'M', 'X'])
    print(le.classes_)
    print(le.transform(['X', 'C', 'M', 'No']))
    flares_numbers = le.transform(data_numFlareStamp_2016may['FlareClass'])
    flares_numbers = pd.DataFrame(flares_numbers)
    flares_numbers = flares_numbers.replace(0, 5)
    flares_numbers = flares_numbers.replace(2, 0)
    flares_numbers = flares_numbers.replace(1, 2)
    flares_numbers = flares_numbers.replace(5, 1)
    print(list(flares_numbers).count(5))
    flares_numbers_out = le.transform(out)
    flares_numbers_out = pd.DataFrame(flares_numbers_out)
    flares_numbers_out = flares_numbers_out.replace(0, 5)
    flares_numbers_out = flares_numbers_out.replace(2, 0)
    flares_numbers_out = flares_numbers_out.replace(1, 2)
    flares_numbers_out = flares_numbers_out.replace(5, 1)

    fig, ax = plt.subplots(1,1)
    plt.plot(data_numFlareStamp_2016may['Timestamp'], data_numFlareStamp_2016may['FlareClass'], label='True values')
    plt.plot(data_numFlareStamp_2016may['Timestamp'], out, label='Prediction')
    plt.setp(ax.xaxis.get_majorticklabels(), rotation=-45, ha="left", rotation_mode="anchor") 
    x = np.arange(0, 4, 1)
    print(x)
    Flares = ['No', 'C', 'M', 'X']
    plt.yticks(x, Flares)
    plt.legend()
    plt.show()
    """

def nbClusters_kMeans(data):
    """
    Determine optimal number of clusters for K-means, with a knee plot.
    """
    inertia_knee = []
    cluster_numbers = np.arange(2, 8, 1)

    for nbClusters in cluster_numbers:
        kmeansData_fit = KMeans(n_clusters=nbClusters, random_state=SEED, n_init=3).fit(data)
        # knee-plot
        inertia = kmeansData_fit.inertia_ #=SSE
        inertia_knee.append(inertia)

    #Make knee plot, this can be plotted if you want a visual representation of the knee plot
    inertia_knee = np.array(inertia_knee) / max(inertia_knee)
    # line from max to max
    x = np.arange(min(cluster_numbers), max(cluster_numbers) + 0.01, 0.01)
    slope = -(max(inertia_knee) - min(inertia_knee)) / (max(cluster_numbers) - min(cluster_numbers))
    b = 1 - min(x) * slope
    y = x * slope + b 

    # distance between lines and point
    distances = []
    for iner, nbcluster in zip(inertia_knee, cluster_numbers):
        p1 = np.array([max(inertia_knee), min(x)])
        p2 = np.array([min(inertia_knee), max(x)])
        p3 = np.array([iner, nbcluster])
        d = np.cross(p2 - p1, p3 - p1) / np.linalg.norm(p2 - p1)
        distances.append(d)

    optimalNb = cluster_numbers[np.where(distances == max(distances))][0]
    return optimalNb


def kMeans(data, flares, string, output_dir, save): #all data used, no split in training, validation and test
    """
    K-means algorithm.
    """
    ### DETERMINE OPTIMAL NUMBER OF CLUSTERS ###
    optimalNb = nbClusters_kMeans(data)

    #Optimal k-means clustering
    clustering_fit = KMeans(n_clusters=optimalNb, random_state=SEED, n_init=3).fit(data)
    clustering_predict = clustering_fit.predict(data)
    clustering = pd.DataFrame(clustering_predict, columns=['ClusterNumber'])
    df_cluster_withFlares = pd.concat([clustering, flares], axis=1)
    printNbFlaresInCluster(df_cluster_withFlares, string, 'kMeans', output_dir, save)
    evaluateUnsClustering(data, clustering_predict)


def SelBest(arr: list, X: int) -> list:
    """
    Sort the array and select the X best inputs.
    """
    dx = np.argsort(arr)[:X]
    return arr[dx]


def GMM_optimalNbClusters(data_train, data_val, output_dir, save):
    """
    Determine optimal number of clusters for GMM.
    """
    n_clusters = np.arange(1, 6) #11)
    bics = []
    bics_error = []
    iterations = 5 #10
    for n in n_clusters:
        print(n)
        tmp_bic = []
        for _ in range(iterations):
            gmm = GaussianMixture(n, n_init=3).fit(data_train)
            tmp_bic.append(gmm.bic(data_val))
        val = np.mean(SelBest(np.array(tmp_bic), int(iterations))) #iterations/5
        err = np.std(tmp_bic)
        bics.append(val)
        bics_error.append(err)
    gradients = np.gradient(bics)
    number = np.argmin(gradients) + 3
    print('nb clusters', number)
    Plotter.BIC_GMM(n_clusters, bics, bics_error, output_dir, save)
    return number


def GMM(data, flares, string, output_dir, save):
    """
    GMM algorithm.
    """
    nb = GMM_optimalNbClusters(data, data, output_dir, save) #unsupervised does not have train and validation data
    gm_fit = GaussianMixture(n_components=nb, random_state=SEED, n_init=3).fit(data)
    gm = gm_fit.predict(data)

    prob = gm_fit.predict_proba(data)
    score = gm_fit.score(data)
    score_sample = gm_fit.score_samples(data)
    print('gmm prob', prob)
    print(np.array(prob).shape) #ik denk (aantal_samples, aantal_clusters)
    print('gmm score', score)
    print('gmm score sample', score_sample)
    print(len(score_sample))


    gm = pd.DataFrame(gm, columns=['ClusterNumber'])
    df_clusters_withFlares = pd.concat([gm, flares], axis=1)
    printNbFlaresInCluster(df_clusters_withFlares, string, 'GMM', output_dir, save)


    sns.histplot(prob[:,0], bins=50, color='b')
    plt.show()
    plt.clf()
    plt.close()
    sns.histplot(prob[:,1], bins=50, color='r')
    plt.show()
    plt.clf()
    plt.close()
    sns.histplot(prob[:,2], bins=50, color='m')
    plt.show()
    evaluateUnsClustering(data, gm)

"""
Print the number of each type of flare in each resulting cluster.
Used for K-means and GMM.
Visualizes the results in a heatmap.
"""
def printNbFlaresInCluster(df_clusters_withFlares, string, method, output_dir, save):
    c1 = pd.DataFrame([df_clusters_withFlares.iloc[i] for i in range(len(df_clusters_withFlares)) if
                       df_clusters_withFlares.iloc[i]['ClusterNumber'] == 3])
    c2 = pd.DataFrame([df_clusters_withFlares.iloc[i] for i in range(len(df_clusters_withFlares)) if
                       df_clusters_withFlares.iloc[i]['ClusterNumber'] == 1])
    c3 = pd.DataFrame([df_clusters_withFlares.iloc[i] for i in range(len(df_clusters_withFlares)) if
                       df_clusters_withFlares.iloc[i]['ClusterNumber'] == 0])
    c4 = pd.DataFrame([df_clusters_withFlares.iloc[i] for i in range(len(df_clusters_withFlares)) if
                       df_clusters_withFlares.iloc[i]['ClusterNumber'] == 2])
    print(c1, c2, c3, c4)

    clusters = []
    for c in [c1, c2, c3, c4]:
        if len(c) > 0:
            clusters.append(c)

    nbNo, nbC, nbM, nbX = np.zeros((len(clusters))), np.zeros((len(clusters))), np.zeros((len(clusters))), np.zeros((len(clusters)))

    matrix_perc2 = np.zeros((len(clusters), 4))
    for index, c in enumerate(clusters):

        for i in range(len(c)):
            if c.iloc[i]['FlareClass'] == 'No':
                nbNo[index] += 1
            if c.iloc[i]['FlareClass'] == 'C':
                nbC[index] += 1
            if c.iloc[i]['FlareClass'] == 'M':
                nbM[index] += 1
            if c.iloc[i]['FlareClass'] == 'X':
                nbX[index] += 1
        print('Cluster ', index + 1, ' ', len(c), int(nbNo[index]), int(nbC[index]), int(nbM[index]), int(nbX[index]))
        matrix_perc2[index, :] =[nbNo[index] / len(c), nbC[index] / len(c), nbM[index] / len(c), nbX[index] / len(c)]

    totalNo = nbNo.sum()
    totalC = nbC.sum()
    totalM = nbM.sum()
    totalX = nbX.sum()
    matrix_perc = np.zeros((len(clusters), 4))
    for i in range(len(clusters)):
        matrix_perc[i,:] = [nbNo[i] / totalNo, nbC[i] / totalC, nbM[i] / totalM, nbX[i] / totalX]

    Plotter.Heatmap_nbFlareClusters(matrix_perc, string, method, 1, output_dir, save)
    Plotter.Heatmap_nbFlareClusters(matrix_perc2, string, method, 2, output_dir, save)

    pd.DataFrame(matrix_perc).to_csv(output_dir+'Matrix1_'+method+'.csv')
    pd.DataFrame(matrix_perc2).to_csv(output_dir+'Matrix2_'+method+'.csv')


def evaluateUnsClustering(data, prediction):
    """
    Evaluation of unsupervised clustering (KNN).
    Calinski-Harabasz and Silhouette score are calculated.
    """
    #Calinski-Harabasz index
    score_CH = sm.calinski_harabasz_score(data, prediction)
    print('Calinski-Harabasz= ', score_CH)
    #Silhouette coefficient
    score_silh = sm.silhouette_score(data, prediction, random_state=SEED)
    print('Silhouette= ', score_silh)