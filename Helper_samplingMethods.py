import pandas as pd
from imblearn.over_sampling import KMeansSMOTE, BorderlineSMOTE

from imblearn.over_sampling import RandomOverSampler, SMOTE, ADASYN, SVMSMOTE
from imblearn.under_sampling import RandomUnderSampler
import numpy as np


#set seed to allow reproducible results
SEED = 1

### UNDERSAMPLING METHODS (NO, C) #####
def Undersample_NoC_Random(data, flares, nbNo, nbC):
    """
    Under-sampling No-flares and C-flares randomly to 6000 samples each.
    """
    print(list(flares).count('X'))
    sampler = RandomUnderSampler(random_state=SEED, sampling_strategy={'No': nbNo, 'C': nbC, 'M': 599, 'X': 37})
    model, flares_model = sampler.fit_resample(data, flares)
    model = pd.DataFrame(model)

    flares_model = pd.DataFrame(flares_model, columns=['FlareClass'])
    df = pd.concat([model, flares_model], axis=1)

    return df



#### OVERSAMPLING METHODS (M,X) ####
def OversampleMX_random(data, flares, nbM, nbX, undersampled=False):
    """
    Method to over-sample M- and X-flares randomly. Not used in the end, because SMOTE is a better alternative.
    """
    if undersampled == False:
        sampler = RandomOverSampler(random_state=SEED, sampling_strategy={'No':49757, 'C':6431, 'M':nbM, 'X':nbX})
    if undersampled == True:
        sampler = RandomOverSampler(random_state=SEED, sampling_strategy={'No':3500, 'C':3500, 'M':nbM, 'X':nbX})
    model, flares_model = sampler.fit_resample(data, flares)
    model = pd.DataFrame(model)

    flares_model = pd.DataFrame(flares_model, columns=['FlareClass'])
    df = pd.concat([model, flares_model], axis=1)

    return df


def OversampleMX_SMOTE(data, flares, nbM, nbX, undersampled=False):
    """
    SMOTE over-sampling to over-sample M- and X-flares to 6000 samples each.
    """
    if undersampled == False:
        sampler = SMOTE(random_state=SEED, sampling_strategy={'No':49757, 'C':6431, 'M':nbM, 'X':nbX})
    if undersampled == True:
        sampler = SMOTE(random_state=3, sampling_strategy={'No':6000, 'C':6000, 'M':nbM, 'X':nbX}, k_neighbors=30)
    model, flares_model = sampler.fit_resample(data, flares)
    model = pd.DataFrame(model)

    flares_model = pd.DataFrame(flares_model, columns=['FlareClass'])
    df = pd.concat([model, flares_model], axis=1)

    return df


def OversampleMX_Adasyn(data, flares, nbM, nbX, undersampled=False):
    if undersampled == False:
        sampler = ADASYN(random_state=SEED, sampling_strategy={'No':49757, 'C':6431, 'M':nbM, 'X':nbX})
    if undersampled == True:
        sampler = ADASYN(random_state=5, sampling_strategy={'No':6000, 'C':6000, 'M':nbM, 'X':nbX}, n_neighbors=30)
    model, flares_model = sampler.fit_resample(data, flares)
    model = pd.DataFrame(model)
    flares_model = pd.DataFrame(flares_model, columns=['FlareClass'])
    df = pd.concat([model, flares_model], axis=1)
    return df

def OversampleMX_BorderlineSMOTE(data, flares, nbM, nbX, undersampled=False):
    if undersampled == False:
        sampler = BorderlineSMOTE(random_state=SEED, sampling_strategy={'No':49757, 'C':6431, 'M':nbM, 'X':nbX})
    if undersampled == True:
        sampler = BorderlineSMOTE(random_state=5, sampling_strategy={'No':6000, 'C':6000, 'M':nbM, 'X':nbX})
    model, flares_model = sampler.fit_resample(data, flares)
    model = pd.DataFrame(model)
    flares_model = pd.DataFrame(flares_model, columns=['FlareClass'])
    df = pd.concat([model, flares_model], axis=1)
    return df

def OversampleMX_SVMSMOTE(data, flares, nbM, nbX, undersampled=False):
    if undersampled == False:
        sampler = SVMSMOTE(random_state=SEED, sampling_strategy={'No':49757, 'C':6431, 'M':nbM, 'X':nbX})
    if undersampled == True:
        sampler = SVMSMOTE(random_state=5, sampling_strategy={'No':6000, 'C':6000, 'M':nbM, 'X':nbX})
    model, flares_model = sampler.fit_resample(data, flares)
    model = pd.DataFrame(model)
    flares_model = pd.DataFrame(flares_model, columns=['FlareClass'])
    df = pd.concat([model, flares_model], axis=1)
    return df

