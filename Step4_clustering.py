#### LOAD PACKAGES ####
import pandas as pd
import Helper_samplingMethods
import Helper_classificationMethods
import os
import Plotter
import numpy as np
import random

#set seed to allow reproducible results
SEED = 1
np.random.seed(SEED)
os.environ['PYTHONHASSEED'] = str(SEED)
random.seed(SEED)

output_dir = 'Data/balanced/'
os.makedirs(output_dir, exist_ok=True)
save=True

#### LOAD DATA ####
data = pd.read_csv('Data/selu_output_hiddenLayer_7_0.1_adam0.0005.csv').drop('Unnamed: 0', axis=1) # X (inputs)
data_flares = pd.read_csv('Data/df_no_outliers_TOTFZ_widened_numFlareStamp.csv')['FlareClass']     # y (labels)

 #Plot the mean and standard deviation of each of the 7 parameters, per flare type.
#Plotter.Mean_sigma(data, data_flares, save)




#### SAMPLING ####
# undersample No and C
underS = Helper_samplingMethods.Undersample_NoC_Random(data, data_flares, 6000, 6000)
# oversample M and X to 6000, 6000
underS_overS_SMOTE6000_adasyn = Helper_samplingMethods.OversampleMX_Adasyn(underS.iloc[:, :-1], 
                                                                           underS.iloc[:, -1], 6000, 6000, True)
underS_overS_SMOTE6000_smote = Helper_samplingMethods.OversampleMX_SMOTE(underS.iloc[:, :-1], underS.iloc[:, -1], 
                                                                         6000, 6000, True)
#underS_overS_SMOTE6000_Kmeanssmote = Helper_samplingMethods.OversampleMX_KmeansSMOTE(underS.iloc[:, :-1], 
#                                                                       underS.iloc[:, -1], 6000, 6000, True)
underS_overS_SMOTE6000_Borderlinesmote = Helper_samplingMethods.OversampleMX_BorderlineSMOTE(underS.iloc[:, :-1], 
                                                                                             underS.iloc[:, -1], 6000, 
                                                                                             6000, True)


Plotter.Density(data, data_flares, underS_overS_SMOTE6000_adasyn, underS_overS_SMOTE6000_smote, 
                underS_overS_SMOTE6000_Borderlinesmote, True)


#### CLUSTERING ####
data_sampled = underS_overS_SMOTE6000_smote
string = 'underS_overS_SMOTE6000'

# KNN
print("------KNN-------")
Helper_classificationMethods.kNN(data, data_flares, data_sampled, string, output_dir, save)

# KMEANS
print("------Kmeans-------")
#Helper_classificationMethods.kMeans(data_sampled.iloc[:,:-1], data_sampled.iloc[:,-1], string, output_dir, save)

# GMM
print("------Unsupervised GMM---------")
#Helper_classificationMethods.GMM(data_sampled.iloc[:, :-1], data_sampled.iloc[:, -1], string, output_dir, save)

"""
matrix1_kmeans = pd.read_csv(output_dir+'Matrix1_kMeans.csv').to_numpy()[:,1:]
matrix2_kmeans = pd.read_csv(output_dir+'Matrix2_kMeans.csv').to_numpy()[:,1:]
matrix1_gmm = pd.read_csv(output_dir+'Matrix1_GMM.csv').to_numpy()[:,1:]
matrix2_gmm = pd.read_csv(output_dir+'Matrix2_GMM.csv').to_numpy()[:,1:]

string='kMeans_GMM'
Plotter.Heatmap_nbFlareClusters_KmeansGMM(matrix1_kmeans, matrix1_gmm, string, 1, output_dir, save)
Plotter.Heatmap_nbFlareClusters_KmeansGMM(matrix2_kmeans, matrix2_gmm, string, 2, output_dir, save)
"""