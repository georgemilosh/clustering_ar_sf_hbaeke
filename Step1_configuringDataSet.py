import pandas as pd
import os
import sklearn.preprocessing


def main():
    """
    Main function, configures the data set.
    - Downloads data set Angryk
    - Merges all the partitions and one-point-in-time data
    - Removes NaN values
    - Splits magnetic field parameters from flare information
    - Standardizes data and under-samples non-flaring active regions to 50 000 samples.
    """
    save=True
    
    #CHANGE THIS PARAMETER, where you saved the data downloaded from Angryk et al., see README.md on how to dowload the data  
    dir = '/media/nas/Public/Data' #ADAPT this to the directory where you downloaded the SWAN data set from Angryk et al.
    SWAN_dir = 'SWAN-SF/dataverse_files/SWAN/' #where the data is located after unzipping the SWAN.tar.gz archive.
    original_data_dir = os.path.join(dir, SWAN_dir)
    source = "Data/AllData_sharp.csv"
    if os.path.exists(source):
        print(f"reading from {source}")
        df_temp = pd.read_csv(source, sep='\t')
        df_sharpnum = df_temp['SHARPnum']
    else:
        print(f"reading from {original_data_dir}")
        df_temp = createInputData(original_data_dir, save)
        df_sharpnum = df_temp['SHARPnum']
    print(f'{len(df_temp)} samples read from ',source)
    print(f'columns: {df_temp.columns}')

    #The 24 magnetic field parameters included in the data set
    param = ['TOTUSJH', 'TOTBSQ', 'TOTPOT', 'TOTUSJZ', 'ABSNJZH', 'SAVNCPP', 'USFLUX', 'TOTFZ', 'MEANPOT',
        'EPSZ', 'SHRGT45', 'MEANSHR', 'MEANGAM', 'MEANGBT', 'MEANGBZ', 'MEANGBH', 'MEANJZH', 'TOTFY',
        'MEANJZD', 'MEANALP', 'TOTFX', 'EPSY', 'EPSX', 'R_VALUE','Timestamp']

    df_temp2, dfsharp, biggestFlare, timestamp = selectRelevantData(df_temp, df_sharpnum, param)
    param = param[:-1] #remove timestamp from the list of parameters
    #df_temp2 = df_temp2.drop("Timestamp", axis=1)
    #dfsharp = dfsharp.drop("Timestamp", axis=1)
    #biggestFlare = biggestFlare.drop("Timestamp", axis=1)
    #df_temp = df_temp.drop("Timestamp", axis=1)
    #df_sharpnum = df_sharpnum.drop("Timestamp", axis=1)
    print(f'{len(df_temp2)} samples selected after running selectRelevantData')

    dfsharp = dfsharp.reset_index(drop=True)
    biggestFlare = biggestFlare.reset_index(drop=True)
    timestamp = timestamp.reset_index(drop=True)
    df_numFlareStamp = pd.concat([dfsharp, biggestFlare, timestamp], axis=1)
        
    df = normalize(df_temp2, param)
    df = pd.concat([df, dfsharp], axis=1)
    if save:
        df.to_csv('Data/df_ALL_standardized.csv')
        df_numFlareStamp.to_csv('Data/df_ALL_standardized_numFlareStamp.csv')

    df, df_numFlareStamp = SampleNoBflares_to50000(df, df_numFlareStamp, save)
    #final data set is saved in : 'Data/Sample_of_ALLData.csv' and 'Data/Sample_of_ALLData_Flare.csv'


def createInputData(dir, save=True):
    """
    The data set is splitted in partitions, based on time. This function concatenates all the partitions into one large data frame.
    """
    partitions = ['partition1', 'partition2', 'partition3', 'partition4', 'partition5']
    df = pd.DataFrame()
    for partition in partitions:
        files = [f for f in os.scandir(dir+ partition) if os.path.isfile(f)]
        print('Number of files in ', partition, ' = ', len(files))
        ### all sharp files at a time ###
        i = 0
        while i < len(files):
            try:
                os.DirEntry = files[i]
                frame = pd.read_csv(os.DirEntry.path, sep='\t')
                #add column with the sharpnum to df
                sharpnum = files[i].name.split('.')[0]
                sharplist = [str(sharpnum)]*len(frame)
                frame = frame.assign(SHARPnum=sharplist)
                df = pd.concat([df, frame], axis=0, ignore_index=True)
                i += 1
            except StopIteration:
                break
    if save:
        df.to_csv('Data/AllData_sharp.csv', sep='\t')
    return df


def removeNaNvalues(df, dfsharp):
    """
    Removes all samples where one of the features contains a NaN value.
    """
    # remove data where Nan values exist
    data_to_remove = []
    for i in range(len(df)):
        if df.iloc[i].isnull().values.any():
            data_to_remove.append(i)
    # drop the rows with nan values
    df = df.drop(data_to_remove)
    dfsharp = dfsharp.drop(data_to_remove)
    return df, dfsharp


def selectRelevantData(df, dfsharp, param):
    """
    Select only the relevant data in the data set.
    - Remove samples with NaN values.
    - Select only the relevant magnetic field parameters.
    - Select the biggest flare concurrently occurring at the active region.
    """
    df = df[param+['BFLARE','CFLARE','MFLARE','XFLARE','Timestamp']]
    #dfsharp = dfsharp[param]
    df, dfsharp = removeNaNvalues(df, dfsharp)
    print('NaNs removed')
    print('Number of samples left after removing NaNs = ', len(df))

    df_timestamp = df["Timestamp"]
    # select the columns we want and concatenate them:
    frames = []
    for i in param:
        frames.append(df[i])
    #select for each sample the biggest flare that occurs at the same time of the magnetic field measurement
    biggestFlare = []
    for j in range(len(df)):
        flare = 'No'
        if df.iloc[j]['BFLARE'] > 0:
            flare = 'No'        #merge No flares with B-flares, because we are not interested in these weak flares
        if df.iloc[j]['CFLARE'] > 0:
            flare = 'C'
        if df.iloc[j]['MFLARE'] > 0:
            flare = 'M'
        if df.iloc[j]['XFLARE'] > 0:
            flare = 'X'
        biggestFlare.append(flare)
    biggestFlare = pd.DataFrame(biggestFlare, columns=["FlareClass"])
    df = pd.concat(frames, axis=1)

    
    df = df.drop("Timestamp", axis=1)

    return df, dfsharp, biggestFlare, df_timestamp

def normalize(df, param):      
    """
    Normalize every magnetic field parameters to mean=0 and std=1.
    """
    scaler = sklearn.preprocessing.StandardScaler()
    df_sklearn = scaler.fit_transform(df)
    df_sklearn = pd.DataFrame(df_sklearn, columns=param)
    return df_sklearn

def SampleNoBflares_to50000(df, df_numFlareStamp, save=True):
    """
    As mentioned in the report the number of non-flaring data (both non-flaring and B-flares) is undersampled to 50000 samples.
    """
    Flare = df_numFlareStamp['FlareClass']
    NoB_flares = []
    CMX_flares = []

	#makes a list of the indices of No and B-flares and a separate list of the indices of C, M and X-flares
    for i in range(len(Flare)):
        if Flare[i] == 'No':
            NoB_flares.append(i)
        else:
            CMX_flares.append(i)

    DataCMX = df.drop(NoB_flares)
    FlareCMX = df_numFlareStamp.drop(NoB_flares)

    DataNoB = df.drop(CMX_flares)
    FlareNoB = df_numFlareStamp.drop(CMX_flares)

    DataNoB_sample = DataNoB.sample(50000, random_state=1)
    FlareNoB_sample = FlareNoB.sample(50000, random_state=1)

    DataSample = pd.concat([DataCMX, DataNoB_sample], axis=0)
    FlareSample = pd.concat([FlareCMX, FlareNoB_sample], axis=0)

    if save:
        DataSample.to_csv('Data/Sample_of_ALLData.csv')
        FlareSample.to_csv('Data/Sample_of_ALLData_Flare.csv')
        
    return DataSample, FlareSample


if __name__ == "__main__":
    main()