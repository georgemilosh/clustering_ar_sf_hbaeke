import matplotlib as mpl
import numpy as np
import pandas as pd
import tensorflow as tf
import os
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from tensorflow.keras import layers, losses, metrics
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l1
import Plotter
import random

mpl.rcParams["axes.labelsize"] = 15

#set seed to allow reproducible results
SEED = 1
np.random.seed(SEED)
os.environ['PYTHONHASSEED'] = str(SEED)
random.seed(SEED)
tf.random.set_seed(SEED)

"""
TO DO
Main function, sparsifying data set with an autoencoder.
 - First run Helper_hyperparametersAE.py to determine the optimal hyperparameters.
 - Split data into training, validation and test set.
 - Apply autoencoder on data set, hidden layer gives the new sparsified features.
 - Two plots generated: pairplot of the resulting features + training versus validation loss.
"""
def main():
    save=True
    output_dir='Data/'
    os.makedirs(output_dir, exist_ok=True)

    #load data
    df = pd.read_csv('Data/output_PCA.csv', sep=',').drop('Unnamed: 0', axis=1)
    df_flare = pd.read_csv('Data/df_no_outliers_TOTFZ_widened_numFlareStamp.csv')["FlareClass"]
    df_numpy = pd.DataFrame.to_numpy(df)



    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3, mode='min', verbose=1, min_delta=1e-4) #added early stopping, does not change anything

    
    #split data into train, validation and test set
    train_ratio = 0.6
    validation_ratio = 0.2
    test_ratio = 0.2
    # train is now 60% of the entire data set
    x_train, x_test, flare_train, flare_test = train_test_split(df, df_flare, test_size=1 - train_ratio, random_state=1,
                                                                stratify=df_flare)
    # test is now 20% of the initial data set
    # validation is now 20% of the initial data set
    x_val, x_test, flare_val, flare_test = train_test_split(x_test, flare_test,
                                                            test_size=test_ratio / (test_ratio + validation_ratio),
                                                            random_state=1, stratify=flare_test)
    x_train = pd.DataFrame.to_numpy(x_train)
    x_val = pd.DataFrame.to_numpy(x_val)
    x_test = pd.DataFrame.to_numpy(x_test)
    
    #split = StratifiedShuffleSplit(n_splits=5, random_state=0, test_size=0.2) #added k-fold cross-validation, does not really change anything
    #print('nb of splits: ', split.get_n_splits(df, df_flare))
    #all_loss = np.empty((0,4))
    #for i, (train_index, val_index) in enumerate(split.split(df, df_flare)):
     #   print(f"Fold {i}:")
        
     #   x_train = df.iloc[train_index]
     #   x_val = df.iloc[val_index]
     #   x_train = pd.DataFrame.to_numpy(x_train)
     #   x_val = pd.DataFrame.to_numpy(x_val)

    #optimal parameters found for the sparse autoencoder
    latent_dim = 7
    regularizer = 0.1 #0.03
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.0005)
    optiName = 'adam0.0005'
    nb_epochs = 300

    autoencoder = Autoencoder(latent_dim, regularizer)

    autoencoder.compile(optimizer=optimizer, loss=losses.MeanSquaredError(),
                        metrics=[metrics.MeanSquaredError()]) #, R2, MSE_self])

    history = autoencoder.fit(x_train, x_train,
                                epochs=nb_epochs,
                                shuffle=True,
                                validation_data=(x_val, x_val),
                                callbacks=[callback])

    #encode and decode the validation data
    encoded_imgs = autoencoder.encoder(x_val).numpy()
    decoded_imgs = autoencoder.decoder(encoded_imgs).numpy()

    #encode and decode the training data
    encoded_imgs_train = autoencoder.encoder(x_train).numpy()
    decoded_imgs_train = autoencoder.decoder(encoded_imgs_train).numpy()

    names_features = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7']

    #the data from the hidden layer is the data in the new, sparser parameter space
    encoded_imgs_all = pd.DataFrame(autoencoder.encoder(df_numpy).numpy(), columns=names_features[:latent_dim])
    if save:
        encoded_imgs_all.to_csv(output_dir+'selu_output_hiddenLayer_' + str(latent_dim) + '_' + str(
        regularizer) + '_' + optiName + '.csv')

    #Evaluate on test data
    test_losses = autoencoder.evaluate(x_val, x_val)

    print('test loss, test acc: ', autoencoder.evaluate(x_val, x_val)) #was x_test, x_test #outputs losses.MSE(), metrics.MSE(), R2, MSE_self

    #Plotter.Pairgrid(encoded_imgs_all, output_dir, latent_dim, regularizer, optiName, save)
    Plotter.Loss(output_dir, latent_dim, regularizer, optiName, nb_epochs, history, save)
    #output of autoencoder saved in: 'Data/selu_output_hiddenLayer_7_0.03_adam0.0005.csv'


    autoencoder.save('Data/Model_AE', save_traces=True)

"""
Evaluation metric
"""
def R2(y_true, y_pred):
    total_error = tf.reduce_sum(tf.square(tf.math.subtract(y_true, tf.reduce_mean(y_true))))
    unexplained_error = tf.reduce_sum(tf.square(tf.math.subtract(y_true, y_pred)))
    R_squared = tf.math.subtract(np.float(1), tf.math.divide(unexplained_error, total_error))
    return R_squared

"""
Evaluation metric
"""
def MSE_self(y_true, y_pred):
    return tf.reduce_mean(tf.square(tf.math.subtract(y_true, y_pred)))

"""
AutoEncoder model
"""
class Autoencoder(Model):
    def __init__(self, latent_dim, regularizer):
        super(Autoencoder, self).__init__()
        self.latent_dim = latent_dim
        self.encoder = tf.keras.Sequential([
            layers.Dense(latent_dim, activity_regularizer=l1(regularizer), activation='selu')
        ])
        self.decoder = tf.keras.Sequential([
            layers.Dense(5)
        ])

    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded
    
    #def get_config(self):
    #    return {"hidden_units": self.encoder}

    #@classmethod
    #def from_config(cls, config):
    #    return cls(**config)

if __name__ == "__main__":
    main()