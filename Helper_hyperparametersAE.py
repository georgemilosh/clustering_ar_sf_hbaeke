import matplotlib as mpl
import numpy as np
import pandas as pd
import tensorflow as tf
import os
from sklearn.model_selection import train_test_split
from tensorflow.keras import layers, losses, metrics
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l1
from tensorflow.keras import backend as K
import Plotter
import random

mpl.rcParams["axes.labelsize"] = 15

#set seed to allow reproducible results
SEED = 1
np.random.seed(SEED)
os.environ['PYTHONHASSEED'] = str(SEED)
random.seed(SEED)
tf.random.set_seed(SEED)


def main():
    """
    Main function, determine hyperparameters for autoencoder of Step3.
    """
    save=True
    output_dir='Data/optimization_AE/'
    os.makedirs(output_dir, exist_ok=True)

    #load data
    df = pd.read_csv('Data/output_PCA.csv', sep=',').drop('Unnamed: 0', axis=1)
    df_flare = pd.read_csv('Data/df_no_outliers_TOTFZ_widened_numFlareStamp.csv')["FlareClass"]
    df_numpy = pd.DataFrame.to_numpy(df)

    #split data into train, validation and test set
    train_ratio = 0.6
    validation_ratio = 0.2
    test_ratio = 0.2
    # train is now 60% of the entire data set
    x_train, x_test, _flare_trainflare_train, flare_test = train_test_split(df, df_flare, test_size=1 - train_ratio, random_state=SEED,
                                                                stratify=df_flare)
    # test is now 20% of the initial data set
    # validation is now 20% of the initial data set
    x_val, x_test, _flare_val, flare_test = train_test_split(x_test, flare_test,
                                                            test_size=test_ratio / (test_ratio + validation_ratio),
                                                            random_state=SEED, stratify=flare_test)
    x_train = pd.DataFrame.to_numpy(x_train)
    x_val = pd.DataFrame.to_numpy(x_val)
    x_test = pd.DataFrame.to_numpy(x_test)

    
    latent_dim = 7

    ### Optimization of learning rate - exponential decay of learning rate ###
    lr_schedule = tf.keras.callbacks.LearningRateScheduler(lambda epoch: 1e-8*10**(epoch/20))
    regularizer = 0.03
    nb_epochs = 160
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.0005)

    #saving activations
    activations_list = []
    loss_validation = []
    activations_callback = tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda batch, logs: save_activations(autoencoder, activations_list, x_val, regularizer, loss_validation))


    autoencoder = Autoencoder(latent_dim, regularizer)

    autoencoder.compile(optimizer=optimizer, loss=losses.MeanSquaredError(),
                        metrics=[metrics.MeanSquaredError(), R2, MSE_self])

    history = autoencoder.fit(x_train, x_train,
                                epochs=nb_epochs,
                                shuffle=True,
                                validation_data=(x_val, x_val),
                                callbacks=[activations_callback, lr_schedule])

    Plotter.Loss_vs_lr(output_dir, nb_epochs, history, save)


    ### Optimization of learning rate - comparing validation losses ###
    nb_epochs = 80
    for lr, optiName in zip([0.00005, 0.0001, 0.0005, 0.001],
        ['adam0.00005', 'adam0.0001', 'adam0.0005', 'adam0.001']):
        optimizer = tf.keras.optimizers.Adam(learning_rate=lr)

        autoencoder = Autoencoder(latent_dim, regularizer)

        autoencoder.compile(optimizer=optimizer, loss=losses.MeanSquaredError(),
                            metrics=[metrics.MeanSquaredError(), R2, MSE_self])

        history = autoencoder.fit(x_train, x_train,
                                    epochs=nb_epochs,
                                    shuffle=True,
                                    validation_data=(x_val, x_val),
                                    callbacks=[activations_callback])

        #Evaluate on test data'
        print('test loss, test acc: ', autoencoder.evaluate(x_test, x_test)) #outputs losses.MSE(), metrics.MSE(), R2, MSE_self
        Plotter.Loss(output_dir, latent_dim, regularizer, optiName, nb_epochs, history, save)
    Plotter.Loss_diff_lr(output_dir, save)

    ### Results of above 2 optimization procedures gives an optimal learning rate of 0.0005 ###

    ### Optimization of regularization term lambda ###
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.0005)
    optiName = 'adam0.0005'
    for regularizer in [0.1, 0.05, 0.03, 0.01, 0.005, 0.003, 0.001]: #gives 0.03

        #saving activations
        activations_list = []
        loss_validation = []
        activations_callback = tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda batch, logs: save_activations(autoencoder, activations_list, x_val, regularizer, loss_validation))

        autoencoder = Autoencoder(latent_dim, regularizer)

        autoencoder.compile(optimizer=optimizer, loss=losses.MeanSquaredError(),
                            metrics=[metrics.MeanSquaredError(), R2, MSE_self])

        history = autoencoder.fit(x_train, x_train,
                                    epochs=nb_epochs,
                                    shuffle=True,
                                    validation_data=(x_val, x_val),
                                    callbacks=[activations_callback])

        names_features = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7']

        encoded_imgs_all = pd.DataFrame(autoencoder.encoder(df_numpy).numpy(), columns=names_features[:latent_dim])
        #save the data from the hidden nodes
        if save:
            encoded_imgs_all.to_csv(output_dir+'selu_output_hiddenLayer_' + str(latent_dim) + '_' + str(
            regularizer) + '_' + optiName + '.csv')

        #Evaluate on test data'
        print('test loss, test acc: ', autoencoder.evaluate(x_test, x_test)) #outputs losses.MSE(), metrics.MSE(), R2, MSE_self

        Plotter.Pairgrid(encoded_imgs_all, output_dir, latent_dim, regularizer, optiName, save)
        Plotter.Activity_notZero(autoencoder, x_val, output_dir, latent_dim, regularizer, optiName, names_features, save)
        Plotter.Loss(output_dir, latent_dim, regularizer, optiName, nb_epochs, history, save)

    ### Optimal regularization = 0.03 ###



def R2(y_true, y_pred):
    """
    Evaluation metric
    """
    total_error = tf.reduce_sum(tf.square(tf.math.subtract(y_true, tf.reduce_mean(y_true))))
    unexplained_error = tf.reduce_sum(tf.square(tf.math.subtract(y_true, y_pred)))
    R_squared = tf.math.subtract(np.float(1), tf.math.divide(unexplained_error, total_error))
    return R_squared

def MSE_self(y_true, y_pred):
    """
    Evaluation metric
    """
    return tf.reduce_mean(tf.square(tf.math.subtract(y_true, y_pred)))


def save_activations(model, activations_list, x_val, regularizer, loss_validation):
    """
    Save activations of the autoencoder layers.
    This to check that there are no features that always have 0 activation value.
    """
    outputs = [layer.output for layer in model.layers]
    functors = [K.function([model.layers[0].input], [model.layers[0].output])]
    layer_activations = [f([x_val]) for f in functors]
    activations_list.append(layer_activations[0][0])
    loss_val = regularizer*tf.math.reduce_sum(abs(layer_activations[0][0]))/len(x_val)
    loss_validation.append(loss_val)


class Autoencoder(Model):
    """
    AutoEncoder model
    """
    def __init__(self, latent_dim, regularizer):
        super(Autoencoder, self).__init__()
        self.latent_dim = latent_dim
        self.encoder = tf.keras.Sequential([
            layers.Dense(latent_dim, activity_regularizer=l1(regularizer), activation='selu')
        ])
        self.decoder = tf.keras.Sequential([
            layers.Dense(5)
        ])

    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded


if __name__ == "__main__":
    main()