"""Pipeline where you put in new data, which goes through the trained transformation steps and the trained clustering algorithms."""

import pickle
import pandas as pd
import tensorflow as tf
import numpy as np
import random
import matplotlib.pyplot as plt
import matplotlib.dates as mdates


#maybe remove all AR's that show never a flare?


#input is data with 24 magnetic field parameters and numflarestamp
#output is a figure for a certain active region with the predicted and true flare types over time
def main():
    data = pd.read_csv('Data/df_ALL_standardized.csv').drop('Unnamed: 0', axis=1).drop('SHARPnum', axis=1)
    numflarestamp = pd.read_csv('Data/df_ALL_standardized_numFlareStamp.csv').drop('Unnamed: 0', axis=1)

    #select the AR of interest, if num=None an active region is chosen at random
    data, numflarestamp = select_AR(data, numflarestamp, 4097)

    ## Transform : widen distribution
    widened_data = widenDistribution(data)

    ## Transform : CFA
    filename_cfa = 'Data/Model_CFA.sav'
    CFA = pickle.load(open(filename_cfa, 'rb'))
    output_cfa = CFA.transform(widened_data)

    ## Transform : PCA
    filename_pca = 'Data/Model_PCA.sav'
    PCA = pickle.load(open(filename_pca, 'rb'))
    output_pca = PCA.transform(output_cfa)

    ## Transform : sparse autoencoder
    autoencoder = tf.keras.models.load_model('Data/Model_AE') #, custom_objects = {"Autoencoder": Autoencoder})
    output_ae = pd.DataFrame(autoencoder.encoder(output_pca).numpy(), columns=['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7'])
    
    ## Clustering : KNN
    filename_knn = 'Data/Model_KNN.sav'
    KNN = pickle.load(open(filename_knn, 'rb'))
    clustering  = KNN.predict(output_ae) #, numflarestamp['FlareClass'])
    print(clustering)

    plot(numflarestamp, clustering)
    return clustering




def widenDistribution(data):
    data["TOTFX"] = np.log(np.abs(data["TOTFX"]))
    data["TOTFY"] = np.log(np.abs(data["TOTFY"]))
    data["TOTUSJH"] = np.log(data["TOTUSJH"]-min(data["TOTUSJH"])+0.01)
    data["TOTBSQ"] = np.log(data["TOTBSQ"]-min(data["TOTBSQ"])+0.01)
    data["TOTPOT"] = np.log(data["TOTPOT"]-min(data["TOTPOT"])+0.01)
    data["TOTUSJZ"] = np.log(data["TOTUSJZ"]-min(data["TOTUSJZ"])+0.01)
    data["ABSNJZH"] = np.log(data["ABSNJZH"]-min(data["ABSNJZH"])+0.01)
    data["SAVNCPP"] = np.log(data["SAVNCPP"]-min(data["SAVNCPP"])+0.01)
    data["USFLUX"] = np.log(data["USFLUX"]-min(data["USFLUX"])+0.01)
    data["TOTFZ"] = np.log(-data["TOTFZ"]+max(data["TOTFZ"])+0.01)
    data["MEANPOT"] = np.log(data["MEANPOT"] - min(data["MEANPOT"]) + 0.0001)
    return data

def select_AR(data, numFlareStamp, num=None):
    if num == None:
        num = random.choice(list(numFlareStamp['SHARPnum']))

    data_num = data[numFlareStamp['SHARPnum'] == int(num)]
    numFlareStamp_num = numFlareStamp[numFlareStamp['SHARPnum'] == int(num)]

    return data_num, numFlareStamp_num

def plot(numflarestamp, prediction): #some zoom in necessary or timeframe that is shown
    fig, ax = plt.subplots(1,1)
    plt.plot(numflarestamp['Timestamp'], numflarestamp['FlareClass'], label='True values')
    print(numflarestamp['Timestamp'])
    plt.plot(numflarestamp['Timestamp'], prediction, label='Prediction')

    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='left')
    #plt.setp(ax.xaxis.get_majorticklabels(), rotation=-45, ha="left", rotation_mode="anchor") 
    x = np.arange(0, 4, 1)
    Flares = ['No', 'C', 'M', 'X'] #check whether it is in the right order
    plt.yticks(x, Flares)
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()