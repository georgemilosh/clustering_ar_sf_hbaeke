import pandas as pd
import numpy as np
import hdbscan
from factor_analyzer import FactorAnalyzer
import sklearn.decomposition
import Plotter
import os
import random
import joblib
import pickle


#set seed to allow reproducible results
SEED = 1
np.random.seed(SEED)
os.environ['PYTHONHASSEED'] = str(SEED)
random.seed(SEED)



def main():
    """
    Main function, transforming the data set.
    - Removing outliers with HDBSCAN + manually some additional outliers.
    - Widen data distributions of parameters with very narrow distributions.
    Input is the output from Step1_configuringDataSet.py
    """
    save=True
    source = 'Data/Sample_of_ALLData.csv'
    df = pd.read_csv(source).drop(['Unnamed: 0'], axis=1).iloc[:, :-1] #, 'Unnamed: 0.1'], axis=1)
    numFlareStamp = pd.read_csv('Data/Sample_of_ALLData_Flare.csv').iloc[:,1:]

    param = ['TOTUSJH', 'TOTBSQ', 'TOTPOT', 'TOTUSJZ', 'ABSNJZH', 'SAVNCPP', 'USFLUX', 'TOTFZ', 'MEANPOT',
        'EPSZ', 'SHRGT45', 'MEANSHR', 'MEANGAM', 'MEANGBT', 'MEANGBZ', 'MEANGBH', 'MEANJZH', 'TOTFY',
        'MEANJZD', 'MEANALP', 'TOTFX', 'EPSY', 'EPSX', 'R_VALUE']
    
    biggestFlare = numFlareStamp['FlareClass']
    timestamp = numFlareStamp['Timestamp']
    dfsharp = numFlareStamp['SHARPnum']

    #remove outliers with HDBSCAN
    df_noOutliers_HDBSCAN, df_numFlareStamp_noOutliers_HDBSCAN = outliers_HDBSCAN(df, dfsharp, biggestFlare, timestamp, save)

    # remove outliers for MEANPOT
    df_noOutliers, df_noOutliers_numFlareStamp = outliers_MEANPOT(df_noOutliers_HDBSCAN, df_numFlareStamp_noOutliers_HDBSCAN, save)
    
    #shift R_value such that original 0-values are again 0
    df_noOutliers['R_VALUE'] -= np.min(df_noOutliers['R_VALUE'])

    #widen data distributions
    widenedData = widenDistribution(df_noOutliers, save)

    # remove outliers for TOTFZ
    df_noOutliers_widened, df_noOutliers_numFlareStamp = outliers_TOTFZ(widenedData, df_noOutliers_numFlareStamp, save)
    
    #Perform feature reduction with CFA and PCA
    nb = determineNbFactors(df_noOutliers_widened)
    data_CFA = CFA(df_noOutliers_widened, param, nb, save)
    data_PCA = PCA(data_CFA, save)
    #this last data set is now used for the autoencoder, saved in : 'Data/output_PCA.csv'



def outliers_HDBSCAN(df, dfsharp, biggestFlare, df_timestamp, save):
    """
    Outliers are found using HDBSCAN and removed from the data set.
    Following hyperparameters are used: min_cluster_size=38, min_samples=38
    """
    #optimal variables found for HDBSCAN:
    min_cluster_size = 38
    min_samples = 38

    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, min_samples=min_samples, metric='euclidean',
                                gen_min_span_tree=True).fit(df)

    print('number of clusters HDBSCAN = ', max(clusterer.labels_) + 1)
    noise = [i for i in range(len(df)) if (clusterer.labels_[i] == -1)]
    print('number of outlier points HDBSCAN = ', len(noise))

    # Save df without outliers
    df_noOutliers_HDBSCAN = df.drop(noise).reset_index(drop=True)
    dfsharp_noOutliers_HDBSCAN = dfsharp.drop(noise).reset_index(drop=True)
    biggestFlare_noOutliers_HDBSCAN = biggestFlare.drop(noise).reset_index(drop=True)
    timestamp_noOutliers_HDBSCAN = df_timestamp.drop(noise).reset_index(drop=True)

    df_numFlareStamp_noOutliers_HDBSCAN = pd.concat([dfsharp_noOutliers_HDBSCAN, biggestFlare_noOutliers_HDBSCAN, timestamp_noOutliers_HDBSCAN],
                                            axis=1)
    if save:
        df_noOutliers_HDBSCAN.to_csv('Data/df_no_outliers_HDBSCAN_38_38.csv')
        df_numFlareStamp_noOutliers_HDBSCAN.to_csv('Data/df_no_outliers_HDBSCAN_numFlareStamp_38_38.csv')

    return df_noOutliers_HDBSCAN, df_numFlareStamp_noOutliers_HDBSCAN


def outliers_MEANPOT(df, dfFlare, save=True):
    """
    Outliers are found that have a large value of MEANPOT. Those are also removed from the data set.
    """
    df_noOutliers = df[df["MEANPOT"] <= -0.0025]
    indices = list(df_noOutliers.index.values)
    df_noOutliers_numFlareStamp = dfFlare.iloc[indices]
    if save:
        df_noOutliers.to_csv('Data/df_no_outliers_MEANPOT.csv')
        df_noOutliers_numFlareStamp.to_csv('Data/df_no_outliers_MEANPOT_numFlareStamp.csv')
    return df_noOutliers, df_noOutliers_numFlareStamp


def outliers_TOTFZ(df, dfFlare, save=True):
    """
    Outliers are also found and removed for the parameter TOTFZ, after the data was widened.
    """
    df = df.reset_index().drop('index', axis=1)
    dfFlare = dfFlare.reset_index().drop('index', axis=1)
    df_noOutliers_TOTFZ_widened = df[df["TOTFZ"] >= -1.0]
    indices = list(df_noOutliers_TOTFZ_widened.index.values)
    df_numFlareStamp_noOutliers_TOTFZ_widened = dfFlare.iloc[indices]
    if save:
        df_noOutliers_TOTFZ_widened.to_csv('Data/df_no_outliers_TOTFZ_widened.csv')
        df_numFlareStamp_noOutliers_TOTFZ_widened.to_csv('Data/df_no_outliers_TOTFZ_widened_numFlareStamp.csv') 
    return df_noOutliers_TOTFZ_widened, df_numFlareStamp_noOutliers_TOTFZ_widened


def CFA(df, param, nb, save=True):
    """_
    CFA to go from 24 parameters to 5 parameters, still containing most of the information.
    """
    factors = FactorAnalyzer(n_factors=nb, rotation=None, method='ml')
    factors.fit(df)
    output_cfa = factors.transform(df)
    output_cfa = pd.DataFrame(output_cfa, columns=['F1', 'F2', 'F3', 'F4', 'F5'])
    if save:
        output_cfa.to_csv('Data/outputCFA.csv')
        filename = 'Data/Model_CFA.sav'
        pickle.dump(factors, open(filename, 'wb'))

    Plotter.HeatmapCFA(factors, df, param, save)
    return output_cfa

def determineNbFactors(df):    
    """
    Horn analysis to determine the number of factors for CFA.
    """
    fa = FactorAnalyzer(n_factors=24, rotation=None, method='ml')
    fa.fit(df)
    original_eigv, common_factor_eigv = fa.get_eigenvalues()

    i = 0
    eigenvalue_mean = np.zeros(original_eigv.shape)
    eigenvalue_q = np.zeros((original_eigv.shape[0], 100))
    while i < 100:
        A = np.random.normal(0, 1, df.shape)
        fa_r = FactorAnalyzer(n_factors=24, rotation=None, method='ml')
        fa_r.fit(A)
        original_eigv_r, common_factor_eigv_r = fa_r.get_eigenvalues()
        eigenvalue_mean += original_eigv_r
        eigenvalue_q[:, i] = original_eigv_r
        i += 1
    eigenvalue_mean = eigenvalue_mean / 100

    eigenvalue_q95 = []
    for i in range(24):
        a = np.quantile(eigenvalue_q[i, :], 0.95)
        eigenvalue_q95.append(a)

    nb = 0
    for i in range(24):
        if original_eigv[i] > eigenvalue_mean[i]:
            nb +=1
    print(nb)
    return nb

def PCA(data, save=True):
    """
    PCA to transform the parameters from CFA to have the maximal amount of variance in the features.
    """
    pca = sklearn.decomposition.PCA()
    pca.fit(data)
    output_pca = pca.transform(data)
    output_pca = pd.DataFrame(output_pca, columns=['PC1', 'PC2', 'PC3', 'PC4', 'PC5'])
    if save:
        output_pca.to_csv('Data/output_PCA.csv')
        filename = 'Data/Model_PCA.sav'
        pickle.dump(pca, open(filename, 'wb'))

    return output_pca

def widenDistribution(data, save):
    """
    Widening the data distributions.
    The optimal transformations are manually defined.
    """
    data["TOTFX"] = np.log(np.abs(data["TOTFX"]))
    data["TOTFY"] = np.log(np.abs(data["TOTFY"]))
    data["TOTUSJH"] = np.log(data["TOTUSJH"]-min(data["TOTUSJH"])+0.01)
    data["TOTBSQ"] = np.log(data["TOTBSQ"]-min(data["TOTBSQ"])+0.01)
    data["TOTPOT"] = np.log(data["TOTPOT"]-min(data["TOTPOT"])+0.01)
    data["TOTUSJZ"] = np.log(data["TOTUSJZ"]-min(data["TOTUSJZ"])+0.01)
    data["ABSNJZH"] = np.log(data["ABSNJZH"]-min(data["ABSNJZH"])+0.01)
    data["SAVNCPP"] = np.log(data["SAVNCPP"]-min(data["SAVNCPP"])+0.01)
    data["USFLUX"] = np.log(data["USFLUX"]-min(data["USFLUX"])+0.01)
    data["TOTFZ"] = np.log(-data["TOTFZ"]+max(data["TOTFZ"])+0.01)
    data["MEANPOT"] = np.log(data["MEANPOT"] - min(data["MEANPOT"]) + 0.0001)

    if save:
        data.to_csv('Data/df_widenedData.csv')
    return data


if __name__ == "__main__":
    main()
